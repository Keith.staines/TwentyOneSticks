//
//  TwentyOneSticksGameLogicUnitTests.swift
//  TwentyOneSticksGameLogicUnitTests
//
//  Created by Keith Staines on 07/08/2021.
//

import XCTest
@testable import TwentyOneSticks

class TwentyOneSticksGameLogicUnitTests: XCTestCase {

    func test_default_initializer() throws {
        let sut = try SticksGameLogic()
        XCTAssertEqual(sut.numberOfSticksRemaining, 21)
        XCTAssertEqual(sut.maxNumberRemovablePerTurn, 3)
        XCTAssertEqual(sut.gameState, SticksGameLogic.GameState.inProgress)
    }
    
    func test_initializer_specifying_legal_values() throws {
        let sut = try SticksGameLogic(numberOfSticks: 7, maxNumberRemovablePerTurn: 2)
        XCTAssertEqual(sut.numberOfSticksRemaining, 7)
        XCTAssertEqual(sut.maxNumberRemovablePerTurn, 2)
        XCTAssertEqual(sut.gameState, SticksGameLogic.GameState.inProgress)
    }
    
    func test_initializer_specifying_illegal_numberOfSticks() {
        XCTAssertThrowsError(try SticksGameLogic(numberOfSticks: 0, maxNumberRemovablePerTurn: 2)) { error in
            XCTAssertEqual(error as! SticksGameLogic.GameLogicError, SticksGameLogic.GameLogicError.invalidGame)
        }
    }
    
    func test_initializer_specifying_illegal_maximumNumberRemovablePerTurn_more_than_numberOfSticks() {
        XCTAssertThrowsError(try SticksGameLogic(numberOfSticks: 21, maxNumberRemovablePerTurn: 22)) { error in
            XCTAssertEqual(error as? SticksGameLogic.GameLogicError, SticksGameLogic.GameLogicError.invalidGame)
        }
    }
    
    func test_initializer_specifying_zero_maximumNumberRemovablePerTurn() {
        XCTAssertThrowsError(try SticksGameLogic(numberOfSticks: 21, maxNumberRemovablePerTurn: 0)) { error in
            XCTAssertEqual(error as! SticksGameLogic.GameLogicError, SticksGameLogic.GameLogicError.invalidGame)
        }
    }
    
    func test_gameplay() throws {
        var sut = try SticksGameLogic(numberOfSticks: 3, maxNumberRemovablePerTurn: 2)
        try sut.remove(2)
        XCTAssertEqual(sut.gameState, SticksGameLogic.GameState.inProgress)
        try sut.remove(1)
        XCTAssertEqual(sut.gameState, SticksGameLogic.GameState.gameOver)
        XCTAssertThrowsError(try sut.remove(1)) { error in
            XCTAssertEqual(error as? SticksGameLogic.GameLogicError, SticksGameLogic.GameLogicError.gameIsOver)
        }
    }

}
