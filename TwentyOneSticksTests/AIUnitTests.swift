//
//  AIUnitTests.swift
//  AIUnitTests
//
//  Created by Keith Staines on 07/08/2021.
//

import XCTest
@testable import TwentyOneSticks

class AIUnitTests: XCTestCase {
    
    func test_smart_AI() throws {
        let sut = SticksSmartAI()
        XCTAssertEqual(sut.getTurn(numberRemaining: 1), 1)
        XCTAssertEqual(sut.getTurn(numberRemaining: 2), 1)
        XCTAssertEqual(sut.getTurn(numberRemaining: 3), 2)
        XCTAssertEqual(sut.getTurn(numberRemaining: 4), 3)
    }

    func test() throws {
        let p1 = SticksSmartAI()
        let p2 = SticksSmartAI()
        var logic = try SticksGameLogic()
        let runner = GameRunner(firstPlayer: p1, secondPlayer: p2)
        logic = try runner.run(logic: logic)
        XCTAssertEqual(logic.gameState, SticksGameLogic.GameState.gameOver)
        XCTAssertEqual(runner.winner, 1)
    }
}

class GameRunner {
    let players: [SticksAI]
    private (set) var winner: Int?
    private (set) var nextTurnIndex: Int = 0
    
    
    
    init(firstPlayer: SticksAI, secondPlayer: SticksAI) {
        players = [firstPlayer, secondPlayer]
    }
    
    func run(logic: SticksGameLogic) throws -> SticksGameLogic {
        var logic = logic
        guard logic.gameState != SticksGameLogic.GameState.gameOver else {
            winner = nextTurnIndex
            return logic
        }
        let player = players[nextTurnIndex]
        let number = player.getTurn(numberRemaining: logic.numberOfSticksRemaining)
        try logic.remove(number ?? 0)
        nextTurnIndex = incrementTurn()
        return try run(logic: logic)
    }
    
    func incrementTurn() -> Int {
        (nextTurnIndex + 1) % 2
    }

}



