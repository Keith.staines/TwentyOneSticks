//
//  ContentView.swift
//  TwentyOneSticks
//
//  Created by Keith Staines on 01/08/2021.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        VStack {
            Text("Play 21 sticks!")
                .padding()
            let player1 = HumanPlayer(nickname: "player1")
            let player2 = HumanPlayer(nickname: "player2")
            let game = try! SticksGame(numberOfSticks: 21, maximumToRemove: 3, player1: player1, player2: player2)
            let vm = TwentyOneSticksGameViewModel(game)
            TwentyOneSticksGameView(viewModel: vm)
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
