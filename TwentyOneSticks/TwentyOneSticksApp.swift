//
//  TwentyOneSticksApp.swift
//  TwentyOneSticks
//
//  Created by Keith Staines on 01/08/2021.
//

import SwiftUI

@main
struct TwentyOneSticksApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
