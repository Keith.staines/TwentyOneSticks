//
//  TwentyOneSticksGameView.swift
//  TwentyOneSticksGameView
//
//  Created by Keith Staines on 01/08/2021.
//

import SwiftUI

struct TwentyOneSticksGameView: View {
    
    @ObservedObject var viewModel: TwentyOneSticksGameViewModel
    var body: some View {
        VStack {
            stickView
            Spacer()
            switch viewModel.gameState {
            case .waitingToStart:
                startButton
            case .inProgress:
                nextTurnView
                Spacer()
                playingControls
            case .complete:
                victoryView
            }
            Spacer()
        }
    }
    
    var nextTurnView: some View {
        Text("It is \(viewModel.nextPlayerString)'s turn")
    }
    
    var stickView: some View {
        Text(viewModel.sticksRemaining)
            .font(.largeTitle)
    }
    
    var victoryView: some View {
        VStack {
            Text("VICTORY!")
            Spacer()
            Text(viewModel.winnerString)
            Spacer()
            playAgainView
        }
    }
    
    var playAgainView: some View {
        Button("Play again?") {
            try? viewModel.newGame()
        }
    }
    
    var startButton: some View {
        Button("START") {
            viewModel.startGame()
        }
    }
    
    var playingControls: some View {
        HStack {
            Spacer()
            Text("Remove")
            Spacer()
            Button("1") {
                viewModel.remove(number: 1)
            }
            Spacer()
            Button("2") {
                viewModel.remove(number: 2)
            }
            Spacer()
            Button("3") {
                viewModel.remove(number: 3)
            }
            Spacer()
        }
    }
}

struct TwentyOneSticksGameView_Previews: PreviewProvider {
    static var previews: some View {
        let player1 = HumanPlayer(nickname: "player1")
        let player2 = HumanPlayer(nickname: "player2")
        let game = try! SticksGame(numberOfSticks: 21, maximumToRemove: 3, player1: player1, player2: player2)
        let viewModel = TwentyOneSticksGameViewModel(game)
        TwentyOneSticksGameView(viewModel: viewModel)
    }
}
