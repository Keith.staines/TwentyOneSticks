//
//  TwentyOneSticksViewModel.swift
//  TwentyOneSticksViewModel
//
//  Created by Keith Staines on 01/08/2021.
//

import SwiftUI

class TwentyOneSticksGameViewModel: ObservableObject {
    
    @Published private var game: SticksGame
    
    var sticksRemaining: String { String(game.sticksRemaining) }
    var gameState: SticksGame.GameState { game.gameState }
    var firstPlayer: HumanPlayer?
    
    var winnerString: String {
        guard let winner = game.winningPlayer, let loser = game.losingPlayer else {
            return "Nobody has won"
        }
        return "\(loser.nickname) was forced to take the last stick\n\(winner.nickname) was the winner!"
    }
    
    var nextPlayerString: String {
        guard let player = game.nextPlayer else { return ""}
        return player.nickname
    }
    
    func startGame() {
        guard game.gameState == SticksGame.GameState.waitingToStart else { return }
        game.start(firstPlayer: firstPlayer ?? game.players[0])
    }
    
    func newGame() throws {
        let newfirstPlayer = game.otherPlayer(thisPlayer: game.firstPlayer)
        let newSecondPlayer = game.otherPlayer(thisPlayer: newfirstPlayer)
        let newGame = try SticksGame(numberOfSticks: 21, maximumToRemove: 3, player1: newfirstPlayer, player2: newSecondPlayer)
        game = newGame
    }
    
    func remove(number: Int) {
        do {
            try game.removeSticks(number: number)
        } catch {
            print("Error \(error)")
        }
    }
    
    init(_ game: SticksGame) {
        self.game = game
    }
    
}
