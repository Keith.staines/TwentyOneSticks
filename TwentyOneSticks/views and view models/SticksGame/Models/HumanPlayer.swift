//
//  Player.swift
//  Player
//
//  Created by Keith Staines on 07/08/2021.
//

import Foundation

protocol GamePosition {}
protocol Move {}

protocol MoveMaker {
    var makeMove: MakeMove? { get }
}

typealias MakeMove = (state: GamePosition, move: (Move) -> Void)

struct HumanPlayer: MoveMaker, Identifiable, Codable {
    
    let id: UUID
    let nickname: String
    var makeMove: (state: GamePosition, move: (Move) -> Void)?
    
    init(id: UUID = UUID(), nickname: String) {
        self.id = id
        self.nickname = nickname
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case nickname
    }
}

extension HumanPlayer: Equatable {
    static func == (lhs: HumanPlayer, rhs: HumanPlayer) -> Bool {
        lhs.id == rhs.id
    }
}

extension HumanPlayer: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}


