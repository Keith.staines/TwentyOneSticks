//
//  SticksGameLogic.swift
//  SticksGameLogic
//
//  Created by Keith Staines on 07/08/2021.
//

import Foundation

struct SticksGameLogic: Codable {

    var numberOfSticksRemaining: Int
    let maxNumberRemovablePerTurn: Int
    
    init(numberOfSticks: Int = 21, maxNumberRemovablePerTurn: Int = 3) throws {
        guard numberOfSticks > 0 else { throw GameLogicError.invalidGame}
        guard maxNumberRemovablePerTurn > 0 && maxNumberRemovablePerTurn <= numberOfSticks else { throw GameLogicError.invalidGame }
        self.numberOfSticksRemaining = numberOfSticks
        self.maxNumberRemovablePerTurn = maxNumberRemovablePerTurn
    }
    
    mutating func remove(_ number: Int) throws {
        guard numberOfSticksRemaining > 0 else { throw GameLogicError.gameIsOver }
        guard number > 0 else { throw GameLogicError.removingTooFewSticks }
        guard number <= numberOfSticksRemaining else { throw GameLogicError.removingTooManySticks }
        numberOfSticksRemaining -= number
    }
    
    enum GameState {
        case invalid
        case inProgress
        case gameOver
    }
    
    var gameState: GameState {
        switch numberOfSticksRemaining {
        case 0: return .gameOver
        case 1...: return .inProgress
        default: return .invalid
        }
    }
    
    enum GameLogicError: Error {
        case invalidGame
        case gameIsOver
        case removingTooManySticks
        case removingTooFewSticks
    }
}
