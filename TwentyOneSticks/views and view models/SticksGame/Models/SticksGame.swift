//
//  TwentyOneSicksGame.swift
//  TwentyOneSicksGame
//
//  Created by Keith Staines on 01/08/2021.
//

import Foundation

struct SticksGame: Codable {
    
    private (set) var gameState: GameState
    private (set) var startDate: Date?
    private (set) var endDate: Date?
    private (set) var logic: SticksGameLogic
    private (set) var players: [HumanPlayer]
    private (set) var turns = [Turn]()
    private (set) var nextPlayer: HumanPlayer?
    private (set) var firstPlayer: HumanPlayer?
    
    var numberOfSticks: Int
    var sticksRemaining: Int { logic.numberOfSticksRemaining }
    var sticksRemoved: Int {
        numberOfSticks - sticksRemaining
    }
    private (set) var winningPlayer: HumanPlayer? {
        didSet {
            if winningPlayer != nil {
                endDate = Date()
                gameState = .complete
            }
        }
    }
    
    var losingPlayer: HumanPlayer? {
        guard let winner = winningPlayer else { return nil }
        return otherPlayer(thisPlayer: winner)
    }
    
    enum GameState: Codable {
        case waitingToStart
        case inProgress
        case complete
    }
    
    struct Stick: Identifiable, Codable {
        let id: Int
    }
    
    struct Turn: Codable {
        var date: Date
        var numberRemoved: Int
    }
    
    init(numberOfSticks: Int = 21,
         maximumToRemove: Int = 3,
         player1: HumanPlayer,
         player2: HumanPlayer
    ) throws {
        self.players = [player1, player2]
        self.gameState = .waitingToStart
        self.numberOfSticks = numberOfSticks
        self.logic = try SticksGameLogic(numberOfSticks: numberOfSticks, maxNumberRemovablePerTurn: maximumToRemove)
    }
    
    mutating func start(firstPlayer: HumanPlayer) {
        guard gameState == .waitingToStart else { return }
        self.firstPlayer = firstPlayer
        nextPlayer = firstPlayer
        startDate = Date()
        gameState = .inProgress
    }
    
    func otherPlayer(thisPlayer: HumanPlayer?) -> HumanPlayer {
        if thisPlayer == players[0] { return players[1] }
        return players[0]
    }
    
    enum GameErrors: Error {
        case gameNotStarted
    }
    
    mutating func removeSticks(number: Int) throws {
        guard let _ = startDate else { throw GameErrors.gameNotStarted }
        try logic.remove(number)
        turns.append(Turn(date: Date(), numberRemoved: number))
        nextPlayer = otherPlayer(thisPlayer: nextPlayer)
        winningPlayer = sticksRemaining == 0 ? nextPlayer : nil
    }
    
    
}


