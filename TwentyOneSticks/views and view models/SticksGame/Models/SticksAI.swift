//
//  TwentyOneSticksAI.swift
//  TwentyOneSticksAI
//
//  Created by Keith Staines on 07/08/2021.
//

import Foundation

protocol SticksAI {
    func getTurn(numberRemaining: Int) -> Int?
}

struct SticksRandomAI: SticksAI {
    func getTurn(numberRemaining: Int) -> Int? {
        guard numberRemaining > 0 else { return nil }
        return (1...numberRemaining).randomElement()
    }
}

struct SticksDumbAI: SticksAI {
    func getTurn(numberRemaining: Int) -> Int? {
        guard numberRemaining > 0 else { return nil }
        if numberRemaining > 1 && numberRemaining < 5 {
            return numberRemaining - 1
        } else {
            return 1
        }
    }
}

struct SticksSmartAI: SticksAI {
    func getTurn(numberRemaining: Int) -> Int? {
        guard numberRemaining > 0 else { return nil }
        let reducedNumber = numberRemaining % 4
        switch reducedNumber {
        case 0: return 3
        case 1: return 1
        case 2: return 1
        case 3: return 2
        default: return 1
        }
    }
}
